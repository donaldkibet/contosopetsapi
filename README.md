# Simple ASP.NET CORE Learning Project

A simple asp.net core web api learning project

## Commands

 * run `dotnet run`
 * build `dotnet build`
 * restore `dotnet restore`

 ## API Documentation

 The documentation for the project was built using swagger
 
 To view the documentation </br> run `dotnet run` and visit [http://localhost:{portNumber}/swagger](http://localhost:{portNumber}/swagger)`http://localhost:{portNumber}/swagger`;

 ## Docker

 You can also run this application using docker by running the following commands

 build the image `docker build -t {name of image}`

 run the container `docker run -d -p 8080:80 --name {containerName} {imageName}`
